import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import UserDashboardView from '~/screens/UserDashboard/UserDashboardView'

const Stack = createStackNavigator();

const DashboardStack = () => {
    return (
        <Stack.Navigator initialRouteName="UserDashboard">
            <Stack.Screen name="UserDashboard" component={UserDashboardView} />
        </Stack.Navigator>
    );
}

export default DashboardStack;