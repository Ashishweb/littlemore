import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import LoginView from '~/screens/Login/LoginView'
import ForgotPasswordView from '~/screens/Login/ForgotPasswordView'
import SignupView from '~/screens/Sinup/SignupView';
import Validateprofile from '~/screens/Validateprofile/ValidateprofileView';

const Stack = createStackNavigator();

const LoginStack = () => {
    return (
        <Stack.Navigator initialRouteName="Login">
            <Stack.Screen options={{headerShown: false}} name="Login" component={LoginView} />
            <Stack.Screen name="ForgotView" component={ForgotPasswordView} />
            <Stack.Screen name="SignupView" component={SignupView} />
            <Stack.Screen name="Validateprofile" component={Validateprofile} />
        </Stack.Navigator>
    );
}

export default LoginStack;
