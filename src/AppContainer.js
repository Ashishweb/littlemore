import { NavigationContainer } from '@react-navigation/native';
import React, { Component } from 'react';
import { Provider } from 'react-redux';
import App from './App';
import store from '../src/Store';

export default class AppContainer extends Component {
    render() {
        return (
            <Provider store={store}>
                <NavigationContainer>
                    <App />
                </NavigationContainer>
            </Provider>
        );
    }
}