import React from 'react';
import { View, Text } from 'react-native';
import SignupForm from '~/components/Signup/SignupForm';

const SignupView = () => {
    return (
        <React.Fragment>
            <View style={{flex: 1}}>
                <SignupForm />
            </View>
        </React.Fragment>
    )
}

export default SignupView;