import React from 'react';
import { View, Text } from 'react-native';
import ForgotPasswordForm from '~/components/Login/ForgotPasswordForm';

const ForgotPasswordView = () => {
    return <View style={{flex: 1}}>
        <ForgotPasswordForm />
        </View>
}

export default ForgotPasswordView;