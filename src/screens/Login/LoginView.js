import React from 'react';
import { View, Text } from 'react-native';
import LoginForm from '~/components/Login/LoginForm';

const LoginView = () => {
    return <View style={{flex: 1}}>
        <LoginForm />
    </View>
}

export default LoginView;