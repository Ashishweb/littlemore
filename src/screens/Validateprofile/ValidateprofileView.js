import React from 'react';
import { View, Text } from 'react-native';
import Validateprofile from '~/components/Validateprofile/Validateprofile';

const ValidateprofileView = () => {
    return (
        <React.Fragment>
            <View style={{flex: 1}}>
                <Validateprofile />
            </View>
        </React.Fragment>
    )
}

export default ValidateprofileView;