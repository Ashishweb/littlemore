import { SET_LOGIN_STATUS } from '~/globals/actions';

const initialState = {
    userId: null,
    userData: null,
    loggedIn: false
}

export default function user(state = initialState, action) {
    switch (action.type) {
        case SET_LOGIN_STATUS:
            return { ...state, loggedIn: action.status }
        default:
            return state
    }
}