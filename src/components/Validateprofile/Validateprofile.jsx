import React,{useState,useEffect} from 'react';
import {Text,View,Platform,Image,Button,TouchableOpacity} from 'react-native';
import Container from '../Container';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import CustomButton from '../CustomButton';
import * as ImagePicker from 'expo-image-picker';

const  Validateprofile = ()=>{

    const [image, setImage] = useState(null);

  useEffect(() => {
    (async () => {
      if (Platform.OS !== 'web') {
        const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();
        if (status !== 'granted') {
          alert('Sorry, we need camera roll permissions to make this work!');
        }
      }
    })();
  }, []);

  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });

    console.log(result);

    if (!result.cancelled) {
      setImage(result.uri);
    }
  };

    return(
        <React.Fragment>
             <Container ContainerStyle={{justifyContent:"flex-start",alignItems:"center"}}>
                <View style={{alignItems:'center',paddingBottom:wp('5%'),paddingTop:5}}>
                    <Text style={{fontWeight:'900',fontSize:25,letterSpacing:5}}>Little<Text style={{fontWeight:'bold',fontSize:25,color:'#43a7d8'}}>MORE</Text></Text>
                </View>
              <View style={{marginBottom:10}}>
              <Text style={{fontSize:17,marginBottom:10}}>Validate your profile</Text>
              </View>
               
                <View style={{borderWidth:1,height:hp('55%'),width:wp('80%'),borderRadius:5,}}>
                    <View style={{}}>
                        <TouchableOpacity onPress={pickImage}  style={{borderWidth:0.3,height:hp('48%'),width:wp('65%'),borderRadius:5,margin:25}}>
                            {image && <Image source={{ uri: image }} style={{ height:hp('48%'),width:wp('65%'),borderRadius:5}} />}
                        </TouchableOpacity>
                    </View>
                </View>
                
            </Container>
        </React.Fragment>
    )
}

export default Validateprofile;