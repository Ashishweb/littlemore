import React from 'react';
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from 'react-native-responsive-screen'
import {
  Text,
  TextStyle,
  TouchableOpacity,
  ViewStyle,
  StyleSheet,
} from 'react-native';

const CustomButton = (props) => {
  return (
    <TouchableOpacity
      onPress={props.onPress}
      style={[
        props.disabled ? {...styles.buttonStyle,backgroundColor:'#000'}
       : styles.buttonStyle, props.buttonStyle,
      props.buttonStyle,
    
    ]}
      disabled={props.disabled}>
      
      <Text style={[styles.textStyle, props.textStyle]}>{props.title}</Text>
    </TouchableOpacity>
  );
};
CustomButton.defaultProps = {
  disabled: false,
  IconSize:30,
  IconColor:'#fff',
};
const styles = StyleSheet.create({
  buttonStyle: {
    backgroundColor:'#000',
    borderRadius: 5,
    width:wp('30%'),
    borderWidth:1,
    borderColor:'#fff',
    height:hp('7%'),
    alignItems:'center',
    flexDirection:'row',
    justifyContent:'center',
    marginBottom:10
  },
  textStyle: {
    color: '#0A5688',
    fontSize:18
  },
});
export default CustomButton;