

import { StatusBar } from 'expo-status-bar';
import React,{useState} from 'react';
import { StyleSheet, Text, View, Platform, Button ,TextInput,CheckBox} from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { useSelector, useDispatch } from 'react-redux';
import { SET_LOGIN_STATUS,SET_SIGNUP_STATUS } from '~/globals/actions'
import Container from '../Container';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { Ionicons } from '@expo/vector-icons';
import ActionButton from 'react-native-action-button';
import CustomButton from '../CustomButton';

const SignupForm = () => {
    const navigation = useNavigation();
    const dispatch = useDispatch();
    const [isSelected, setSelection] = useState(false);

    return (
        <Container ContainerStyle={{}}>
             <View style={{alignItems:'center',paddingBottom:wp('5%')}}>
                <Text style={{fontWeight:'bold',fontSize:25}}>Little<Text style={{fontWeight:'bold',fontSize:25,color:'#43a7d8'}}>MORE</Text></Text>
            </View>
            <View style={{alignItems:'center',paddingBottom:wp('5%')}}>
               <Text style={{fontSize:16}}>Set up your Account</Text>
            </View>
            <View style={{paddingTop:10,paddingBottom:hp('1%')}}>
                <Text style={{marginBottom:8,}}>Username</Text>
                    <TextInput style={Style.inputstyle}/>
                        <Text style={{marginBottom:8}}>Password</Text>
                            <TextInput style={Style.inputstyle} />
                                <View style={{flexDirection:'row',justifyContent:"space-between",paddingTop:5,paddingBottom:5}}>
                                            <Text style={Style.label}>Are you registered user?</Text>
                                <Text style={{margin: 6,}} onPress={() => navigation.navigate('Login')}>Login Now</Text>
                            </View>
                            <View style={{flexDirection:'row',justifyContent:"space-between",paddingTop:5}}>
                                <CustomButton textStyle={{color:'#fff',letterSpacing:1}} buttonStyle={Style.Login} title="Submit"
                                    onPress={() => navigation.navigate('Validateprofile')}/>
                                <CustomButton textStyle={{color:'#000',letterSpacing:1}} buttonStyle={Style.Signup} title="Cancel"
                                   onPress={() => navigation.navigate('Login')}/>
                            </View>
            </View>
                <ActionButton 
                    buttonColor="#8a92ff"
                >
                <Ionicons name="airplane-outline" size={40} color="#000" />
            </ActionButton>
        </Container>
    );
}


export default SignupForm;
const Style  = StyleSheet.create({
    inputstyle:{
         height:hp('7%'),
        marginBottom: 4,
        fontSize:16,
        letterSpacing:0.3, 
        borderWidth:1,
        borderRadius:5,
    },
    checkboxContainer: {
        flexDirection: "row",
    },
    label: {
        margin:6,
    },
    Login:{
        width:wp('37%')  
    },
    Signup:{
        width:wp('52%'),
        backgroundColor:'#fff',
        borderWidth:1,
        borderColor:'#000'
    },
    })



  