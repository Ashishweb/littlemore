import * as React from 'react';
import { SafeAreaView,StyleSheet,} from 'react-native'


const Container = (props)=>{
    return(
        <SafeAreaView style={[style.container,props.ContainerStyle,props.Singinput]}>
            {props.children }
        </SafeAreaView> 
    )
}

export default Container;
    const style = StyleSheet.create({
        container:{
            flex:1,
            justifyContent:"center",
            alignItems:"stretch",
            marginHorizontal:12
        }
    })